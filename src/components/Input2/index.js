import React, { useState } from "react";
import "./css/set1.css";
import "./css/demo.css";
import "./css/normalize.css";

export default function Index({ sampledata, i, handleChange }) {

  return (
    <div>

      {/* {sampledata.map((item) => {
          return <span className="input input--jiro">
            <input
              className="input__field input__field--jiro"
              type="text"
              id="input-10"
            />
            <label className="input__label input__label--jiro" for="input-10">
              <span className="input__label-content input__label-content--jiro">
                {item}
              </span>
            </label>
          </span>;
        })} */}

      {(sampledata.length !== i) ? <span className="input input--jiro">
        <input
          className="input__field input__field--jiro"
          type="text"
          id={`input-${i}`}
          name={sampledata[i].name}
          value={sampledata[i].value}
          onChange={(e) => {
            handleChange(e)
          }}
        />
        <label className="input__label input__label--jiro" for={`input-${i}`}>
          <span className="input__label-content input__label-content--jiro">
            {sampledata[i].title}
          </span>
        </label>
      </span> : <div>
        Thank You
      </div>}
    </div>
  );
}
