import React, { useState } from 'react'
import logo from './logo.svg';
import './App.css';
import Main from './components/Input2/index'
import "./components/Input2/css/demo.css";
import "./components/Input2/css/set1.css";
import "./components/Input2/css/normalize.css";

function App() {

  const [sampledata, setSampleData] = useState([{ title: "Enter your name.", value: "", name: 'firstName' }, { title: "Last name.", value: "", name: 'lastName' }, { title: "Email", value: "", name: 'email' }, { title: "Contact Number", value: "", name: 'contact' }]);

  const [i, setI] = useState(0)

  const handleButton = (e) => {
    e.preventDefault()
    var cc = document.getElementById(`input-${i}`)
    console.log('cc', cc.value)
    cc.required = true
    cc.placeholder = sampledata[i] + 'is required'
    if (cc.value == "" || cc.value == null || cc.value == undefined) {

    } else {
      cc.required = false
      setI(i + 1)
      cc.value = ""
    }
  }

  const handleChange = (e) => {
    let lsde = [...sampledata];
    lsde[i] = { ...lsde[i], value: e.target.value }
    setSampleData(lsde);
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <section className="content">
          <h2>HeyTech</h2>
          {(sampledata.length !== i && i > 0) && <input type="button" className="button" value="Back" onClick={() => {
            if (i - 1 > 0) {
              setI(i - 1)
            } else {
              setI(0)
            }
          }}></input>}
          <Main sampledata={sampledata} i={i} handleChange={handleChange} />
          {(sampledata.length !== i) && <input type="button" className="button" value="continue" onClick={(e) => {
            handleButton(e)
          }}></input>}
        </section>
      </header>
    </div>
  );
}

export default App;
